<?php
session_start(); //此示例中要使用session
require_once('config.php');
require_once('kaixin.php');

$kaixin_t=isset($_SESSION['kaixin_t'])?$_SESSION['kaixin_t']:'';

//检查是否已登录

if($kaixin_t!=''){
	$kaixin=new kaixinPHP($kaixin_k, $kaixin_s, $kaixin_t);

	//获取登录用户信息
	$result=$kaixin->me();
	var_dump($result);

	/**
	//access token到期后使用refresh token刷新access token
	$result=$kaixin->access_token_refresh($_SESSION['kaixin_r']);
	var_dump($result);
	**/

	/**
	//发布记录
	$img='http://www.baidu.com/img/baidu_sylogo1.gif';
	$result=$kaixin->records_add('记录内容', $img);
	var_dump($result);
	**/

	/**
	//其他功能请根据官方文档自行添加
	//示例：获取登录用户信息
	$result=$kaixin->api('users/me', array(), 'GET');
	var_dump($result);
	**/

}else{
	//生成登录链接
	$kaixin=new kaixinPHP($kaixin_k, $kaixin_s);
	$login_url=$kaixin->login_url($callback_url, $scope);
	echo '<a href="',$login_url,'">点击进入授权页面</a>';
}
